# Test datasets

Large files in this repository are stored with [git
lfs](https://git-lfs.github.com). Make sure to install it before cloning the
repository!

## 1000 genomes

- Mirrored from https://www.cog-genomics.org/plink/2.0/resources
- Callset 2016-05-05 primary release, build 37
- Split by chromosome
- Singleton variants kept
- No INFO annotations
- KING-based pedigree corrections
- Chromosomes 1 - 22 + X